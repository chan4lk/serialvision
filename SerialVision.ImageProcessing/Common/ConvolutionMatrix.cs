﻿// ***********************************************************************
// Assembly         : SerialVision.ImageProcessing
// Author           : ChandimaRanaweera
// Created          : 10-11-2016
//
// Last Modified By : ChandimaRanaweera
// Last Modified On : 10-11-2016
// ***********************************************************************
// <copyright file="ConvolutionMatrix.cs" company="">
//     Copyright ©  2016
// </copyright>
// <summary></summary>
// ***********************************************************************
namespace SerialVision.ImageProcessing.Common
{
    /// <summary>
    /// The convolution matrix.
    /// </summary>
    public class ConvolutionMatrix
    {
        /// <summary>
        /// Gets or sets the top left.
        /// </summary>
        /// <value>The top left.</value>
        public int TopLeft { get; set; }

        /// <summary>
        /// Gets or sets the top mid.
        /// </summary>
        /// <value>The top mid.</value>
        public int TopMid { get; set; }

        /// <summary>
        /// Gets or sets the top right.
        /// </summary>
        /// <value>The top right.</value>
        public int TopRight { get; set; }

        /// <summary>
        /// Gets or sets the mid left.
        /// </summary>
        /// <value>The mid left.</value>
        public int MidLeft { get; set; }

        /// <summary>
        /// Gets or sets the pixel.
        /// </summary>
        /// <value>The pixel.</value>
        public int Pixel { get; set; }

        /// <summary>
        /// Gets or sets the mid right.
        /// </summary>
        /// <value>The mid right.</value>
        public int MidRight { get; set; }

        /// <summary>
        /// Gets or sets the bottom left.
        /// </summary>
        /// <value>The bottom left.</value>
        public int BottomLeft { get; set; }

        /// <summary>
        /// Gets or sets the bottom mid.
        /// </summary>
        /// <value>The bottom mid.</value>
        public int BottomMid { get; set; }

        /// <summary>
        /// Gets or sets the bottom right.
        /// </summary>
        /// <value>The bottom right.</value>
        public int BottomRight { get; set; }

        /// <summary>
        /// Gets or sets the factor.
        /// </summary>
        /// <value>The factor.</value>
        public int Factor { get; set; }

        /// <summary>
        /// Gets or sets the offset.
        /// </summary>
        /// <value>The offset.</value>
        public int Offset { get; set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="ConvolutionMatrix"/> class.
        /// </summary>
        public ConvolutionMatrix()
        {
            this.TopLeft = this.TopMid = this.TopRight = 0;
            this.MidLeft = this.MidRight = this.Offset = 0;
            this.BottomLeft = this.BottomMid = this.BottomRight = 0;
            this.Pixel = this.Factor = 1;
        }

        /// <summary>
        /// Sets all pixels.
        /// </summary>
        /// <param name="value">The value.</param>
        public void SetAll(int value)
        {
            this.TopLeft = this.TopMid = this.TopRight = value;
            this.MidLeft = this.Pixel = this.MidRight = value;
            this.BottomLeft = this.BottomMid = this.BottomRight = value;
        }
    }
}
