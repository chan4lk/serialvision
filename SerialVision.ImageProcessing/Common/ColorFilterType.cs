﻿// ***********************************************************************
// Assembly         : SerialVision.ImageProcessing
// Author           : ChandimaRanaweera
// Created          : 10-11-2016
//
// Last Modified By : ChandimaRanaweera
// Last Modified On : 10-11-2016
// ***********************************************************************
// <copyright file="ColorFilterType.cs" company="SerialVision">
//     Copyright ©  SerialVision 2016
// </copyright>
// <summary></summary>
// ***********************************************************************

namespace SerialVision.ImageProcessing.Common
{
    /// <summary>
    /// The color filter type.
    /// </summary>
    public enum ColorFilterType
    {
        /// <summary>
        /// The red
        /// </summary>
        Red,

        /// <summary>
        /// The green
        /// </summary>
        Green,

        /// <summary>
        /// The blue
        /// </summary>
        Blue
    }
}
