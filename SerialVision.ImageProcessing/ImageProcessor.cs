﻿// ***********************************************************************
// Assembly         : SerialVision.ImageProcessing
// Author           : ChandimaRanaweera
// Created          : 10-09-2016
//
// Last Modified By : ChandimaRanaweera
// Last Modified On : 10-11-2016
// ***********************************************************************
// <copyright file="Processor.cs" company="SerialVision">
//     Copyright ©  SerialVision 2016
// </copyright>
// <summary></summary>
// ***********************************************************************

using System.Drawing;
using System.Drawing.Imaging;

using Accord.Imaging;
using Accord.Imaging.Filters;

using SerialVision.ImageProcessing.Helpers;
using SerialVision.Common.Exception;

namespace SerialVision.ImageProcessing
{
    using System;
    using SerialVision.Common;

    /// <summary>
    /// Class Processor.
    /// </summary>
    /// <seealso cref="SerialVision.ImageProcessing.IProcessor" />
    public class ImageProcessor : IProcessor
    {
        /// <summary>
        /// The image width
        /// </summary>
        private const int ImageWidth = 100;

        /// <summary>
        /// The image height
        /// </summary>
        private const int ImageHeight = 100;

        /// <summary>
        /// Processes this instance.
        /// </summary>
        /// <param name="source">The source.</param>
        /// <returns>The Bitmap.</returns>
        /// <exception cref="ProcessingException">Image processing Error</exception>
        public Bitmap ProcessActivity(Bitmap source)
        {
            try
            {
                BitmapData bitmapData = source.LockBits(
                new Rectangle(0, 0, source.Width, source.Height),
                ImageLockMode.ReadOnly,
                source.PixelFormat);

                UnmanagedImage unmanagedImage = new UnmanagedImage(bitmapData);
                Grayscale gray = Grayscale.CommonAlgorithms.BT709;
                var image = gray.Apply(unmanagedImage).ToManagedImage();
                var output = (new Bitmap(image)).FitToSize(ImageProcessor.ImageWidth, ImageProcessor.ImageHeight) as Bitmap;
                source.UnlockBits(bitmapData);

                return output;
            }
            catch (Exception innerException)
            {
                throw new ProcessingException("Image processing Error", innerException);
            }
        }

        /// <summary>
        /// Processes the sky line.
        /// </summary>
        /// <param name="source">The source.</param>
        /// <returns>The Bitmap.</returns>
        /// <exception cref="ProcessingException">Image processing Error</exception>
        public Bitmap ProcessSkyLine(Bitmap source)
        {
            try
            {
                BitmapData bitmapData = source.LockBits(
                new Rectangle(0, 0, source.Width, source.Height),
                ImageLockMode.ReadOnly,
                source.PixelFormat);

                UnmanagedImage unmanagedImage = new UnmanagedImage(bitmapData);

                Grayscale gray = Grayscale.CommonAlgorithms.BT709;
                var image = gray.Apply(unmanagedImage);

                Threshold threshold = new Threshold(127);
                threshold.ApplyInPlace(image);

                ResizeBicubic resize = new ResizeBicubic(ImageProcessor.ImageWidth, ImageProcessor.ImageHeight);
                image = resize.Apply(image);

                Crop crop = new Crop(new Rectangle(0, 0, image.Width, image.Height / 2));
                image = crop.Apply(image);

                var output = image.ToManagedImage(true);
                source.UnlockBits(bitmapData);

                return output;
            }
            catch (Exception innerException)
            {
                throw new ProcessingException("Image processing Error", innerException);
            }
        }

        /// <summary>
        /// Processes the height of the building.
        /// </summary>
        /// <param name="source">The source.</param>
        /// <returns>The Bitmap.</returns>
        public Bitmap ProcessBuildingHeight(Bitmap source)
        {
            return this.ProcessActivity(source);
        }

        /// <summary>
        /// Processes the road signs.
        /// </summary>
        /// <param name="source">The source.</param>
        /// <returns>The Bitmap.</returns>
        public Bitmap ProcessRoadSigns(Bitmap source)
        {
            return this.ProcessActivity(source);
        }

        /// <summary>
        /// Processes the color variation.
        /// </summary>
        /// <param name="source">The source.</param>
        /// <returns>The Bitmap.</returns>
        public Bitmap ProcessColorVariation(Bitmap source)
        {
            return this.ProcessActivity(source);
        }

        /// <summary>
        /// Processes the specified source.
        /// </summary>
        /// <param name="source">The source.</param>
        /// <param name="criteria">The criteria.</param>
        /// <returns>The Bitmap.</returns>
        public Bitmap Process(Bitmap source, Criteria criteria)
        {
            switch (criteria)
            {
                case Criteria.Activity:
                    return this.ProcessActivity(source);
                case Criteria.BuildingHeight:
                    return this.ProcessBuildingHeight(source);
                case Criteria.Skyline:
                    return this.ProcessSkyLine(source);
                case Criteria.RoadSigns:
                    return this.ProcessRoadSigns(source);
                case Criteria.ColorVariation:
                    return this.ProcessColorVariation(source);
                default:
                    return source;
            }
        }
    }
}
