﻿// ***********************************************************************
// Assembly         : SerialVision.ImageProcessing
// Author           : ChandimaRanaweera
// Created          : 10-09-2016
//
// Last Modified By : ChandimaRanaweera
// Last Modified On : 10-11-2016
// ***********************************************************************
// <copyright file="IFilter.cs" company="">
//     Copyright ©  2016
// </copyright>
// <summary></summary>
// ***********************************************************************
using Accord.Imaging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace SerialVision.ImageProcessing.Filters
{
    /// <summary>
    /// Interface IFilter
    /// </summary>
    public interface IFilter
    {
        /// <summary>
        /// Applies the in place.
        /// </summary>
        /// <param name="source">The source.</param>
        /// <returns>The Bitmap.</returns>
        Bitmap ApplyInPlace(Bitmap source);

        /// <summary>
        /// Applies the specified source.
        /// </summary>
        /// <param name="source">The source.</param>
        /// <returns>The Bitmap.</returns>
        Bitmap Apply(Bitmap source);
    }
}
