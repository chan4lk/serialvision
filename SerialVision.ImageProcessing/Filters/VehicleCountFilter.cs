﻿// ***********************************************************************
// Assembly         : SerialVision.ImageProcessing
// Author           : ChandimaRanaweera
// Created          : 10-09-2016
//
// Last Modified By : ChandimaRanaweera
// Last Modified On : 10-11-2016
// ***********************************************************************
// <copyright file="VehicleCountFilter.cs" company="">
//     Copyright ©  2016
// </copyright>
// <summary></summary>
// ***********************************************************************
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Accord.Imaging;
using System.Drawing;

namespace SerialVision.ImageProcessing.Filters
{
    /// <summary>
    /// Class VehicleCountFilter.
    /// </summary>
    /// <seealso cref="SerialVision.ImageProcessing.Filters.IFilter" />
    internal class VehicleCountFilter : IFilter
    {
        /// <summary>
        /// Applies the specified source.
        /// </summary>
        /// <param name="source">The source.</param>
        /// <returns>The Bitmap.</returns>
        public Bitmap Apply(Bitmap source)
        {
            return source.Clone() as Bitmap;
        }

        /// <summary>
        /// Applies the in place.
        /// </summary>
        /// <param name="source">The source.</param>
        /// <returns>The Bitmap.</returns>
        public Bitmap ApplyInPlace(Bitmap source)
        {
            return source;
        }
    }
}
