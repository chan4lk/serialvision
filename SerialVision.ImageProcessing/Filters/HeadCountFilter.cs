﻿// ***********************************************************************
// Assembly         : SerialVision.ImageProcessing
// Author           : Chandima Ranaweera
// Created          : 10-09-2016
//
// Last Modified By : Chandima Ranaweera
// Last Modified On : 10-11-2016
// ***********************************************************************
// <copyright file="HeadCountFilter.cs" company="">
//     Copyright ©  2016
// </copyright>
// <summary></summary>
// ***********************************************************************
using System.Drawing;

namespace SerialVision.ImageProcessing.Filters
{
    /// <summary>
    /// Class HeadCountFilter.
    /// </summary>
    /// <seealso cref="SerialVision.ImageProcessing.Filters.IFilter" />
    internal class HeadCountFilter : IFilter
    {
        /// <summary>
        /// Applies the specified source.
        /// </summary>
        /// <param name="source">The source.</param>
        /// <returns>The Bitmap.</returns>
        public Bitmap Apply(Bitmap source)
        {
            return source.Clone() as Bitmap;
        }

        /// <summary>
        /// Applies the in place.
        /// </summary>
        /// <param name="source">The source.</param>
        /// <returns>The Bitmap.</returns>
        public Bitmap ApplyInPlace(Bitmap source)
        {            
            return source;
        }
    }
}
