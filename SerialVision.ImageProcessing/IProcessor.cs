﻿// ***********************************************************************
// Assembly         : SerialVision.ImageProcessing
// Author           : ChandimaRanaweera
// Created          : 10-09-2016
//
// Last Modified By : ChandimaRanaweera
// Last Modified On : 10-11-2016
// ***********************************************************************
// <copyright file="IProcessor.cs" company="">
//     Copyright ©  2016
// </copyright>
// <summary></summary>
// ***********************************************************************
using SerialVision.Common;
using System.Drawing;

namespace SerialVision.ImageProcessing
{
    /// <summary>
    /// Interface IProcessor
    /// </summary>
    public interface IProcessor
    {
        /// <summary>
        /// Processes the activity.
        /// </summary>
        /// <param name="source">The source.</param>
        /// <returns>The Bitmap.</returns>
        Bitmap ProcessActivity(Bitmap source);

        /// <summary>
        /// Processes the sky line.
        /// </summary>
        /// <param name="source">The source.</param>
        /// <returns>The Bitmap.</returns>
        Bitmap ProcessSkyLine(Bitmap source);

        /// <summary>
        /// Processes the height of the building.
        /// </summary>
        /// <param name="source">The source.</param>
        /// <returns>The Bitmap.</returns>
        Bitmap ProcessBuildingHeight(Bitmap source);

        /// <summary>
        /// Processes the road signs.
        /// </summary>
        /// <param name="source">The source.</param>
        /// <returns>The Bitmap.</returns>
        Bitmap ProcessRoadSigns(Bitmap source);

        /// <summary>
        /// Processes the color variation.
        /// </summary>
        /// <param name="source">The source.</param>
        /// <returns>The Bitmap.</returns>
        Bitmap ProcessColorVariation(Bitmap source);

        /// <summary>
        /// Processes the specified source.
        /// </summary>
        /// <param name="source">The source.</param>
        /// <param name="criteria">The criteria.</param>
        /// <returns>The Bitmap.</returns>
        Bitmap Process(Bitmap source, Criteria criteria);
    }
}
