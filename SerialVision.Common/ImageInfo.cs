﻿// ***********************************************************************
// Assembly         : SerialVision.Common
// Author           : ChandimaRanaweera
// Created          : 10-11-2016
//
// Last Modified By : ChandimaRanaweera
// Last Modified On : 10-11-2016
// ***********************************************************************
// <copyright file="ImageInfo.cs" company="SerialVision">
//     Copyright © SerialVision 2016
// </copyright>
// <summary></summary>
// ***********************************************************************

namespace SerialVision.Common
{
    /// <summary>
    /// Class ImageInfo.
    /// </summary>
    public class ImageInfo
    {
        /// <summary>
        /// Gets or sets the URL.
        /// </summary>
        /// <value>The URL.</value>
        public string URL { get; set; }

        /// <summary>
        /// Gets or sets the activity.
        /// </summary>
        /// <value>The activity.</value>
        public int Activity { get; set; }

        /// <summary>
        /// Gets or sets the road signs.
        /// </summary>
        /// <value>The road signs.</value>
        public int RoadSigns { get; set; }

        /// <summary>
        /// Gets or sets the skyline.
        /// </summary>
        /// <value>The skyline.</value>
        public int Skyline { get; set; }

        /// <summary>
        /// Gets or sets the height of the building.
        /// </summary>
        /// <value>The height of the building.</value>
        public int BuildingHeight { get; set; }

        /// <summary>
        /// Gets or sets the color variation.
        /// </summary>
        /// <value>The color variation.</value>
        public int ColorVariation { get; set; }
    }
}
