﻿// ***********************************************************************
// Assembly         : SerialVision.Common
// Author           : ChandimaRanaweera
// Created          : 10-09-2016
//
// Last Modified By : ChandimaRanaweera
// Last Modified On : 10-09-2016
// ***********************************************************************
// <copyright file="ImageProcessResponse.cs" company="">
//     Copyright ©  2016
// </copyright>
// <summary></summary>
// ***********************************************************************
using System.Drawing;

namespace SerialVision.Common
{
    /// <summary>
    /// Class ImageProcessResponse.
    /// </summary>
    public class ImageProcessResponse
    {
        /// <summary>
        /// Gets or sets the activity level.
        /// </summary>
        /// <value>The activity level.</value>
        public int ActivityLevel { get; set; }

        /// <summary>
        /// Gets or sets the road signs level.
        /// </summary>
        /// <value>The road signs level.</value>
        public int RoadSignsLevel { get; set; }

        /// <summary>
        /// Gets or sets the height of the building.
        /// </summary>
        /// <value>The height of the building.</value>
        public int BuildingHeight { get; set; }

        /// <summary>
        /// Gets or sets the color variation.
        /// </summary>
        /// <value>The color variation.</value>
        public int ColorVariation { get; set; }

        /// <summary>
        /// Gets or sets the skyline.
        /// </summary>
        /// <value>The skyline.</value>
        public int Skyline { get; set; }

        /// <summary>
        /// Gets or sets the output.
        /// </summary>
        /// <value>The output.</value>
        public Bitmap Output { get; set; }

    }
}
