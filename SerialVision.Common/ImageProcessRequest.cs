﻿// ***********************************************************************
// Assembly         : SerialVision.Common
// Author           : ChandimaRanaweera
// Created          : 10-09-2016
//
// Last Modified By : ChandimaRanaweera
// Last Modified On : 10-11-2016
// ***********************************************************************
// <copyright file="ImageProcessRequest.cs" company="">
//     Copyright ©  2016
// </copyright>
// <summary></summary>
// ***********************************************************************
using System.Drawing;

namespace SerialVision.Common
{
    /// <summary>
    /// Class ImageProcessRequest.
    /// </summary>
    public class ImageProcessRequest
    {
        /// <summary>
        /// Gets or sets the image.
        /// </summary>
        /// <value>The image.</value>
        public Bitmap Image { get; set; }
    }
}
