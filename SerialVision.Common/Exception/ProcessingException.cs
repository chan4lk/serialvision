﻿// ***********************************************************************
// Assembly         : SerialVision.Common
// Author           : ChandimaRanaweera
// Created          : 10-11-2016
//
// Last Modified By : ChandimaRanaweera
// Last Modified On : 10-11-2016
// ***********************************************************************
// <copyright file="ProcessingException.cs" company="SerialVision">
//     Copyright © SerialVision 2016
// </copyright>
// <summary></summary>
// ***********************************************************************

namespace SerialVision.Common.Exception
{
    using Exception = System.Exception;

    /// <summary>
    /// Class ProcessingException.
    /// </summary>
    /// <seealso cref="System.Exception" />
    public class ProcessingException : Exception
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ProcessingException"/> class.
        /// </summary>
        /// <param name="message">The message.</param>
        public ProcessingException(string message) : base(message)
        {             
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ProcessingException"/> class.
        /// </summary>
        /// <param name="message">The message.</param>
        /// <param name="innerException">The inner exception.</param>
        public ProcessingException(string message, Exception innerException)
            : base(message, innerException)
        {
        }
    }
}
