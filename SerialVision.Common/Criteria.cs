﻿// ***********************************************************************
// Assembly         : SerialVision.Common
// Author           : ChandimaRanaweera
// Created          : 10-11-2016
//
// Last Modified By : ChandimaRanaweera
// Last Modified On : 10-11-2016
// ***********************************************************************
// <copyright file="Criteria.cs" company="">
//     Copyright ©  2016
// </copyright>
// <summary></summary>
// ***********************************************************************
namespace SerialVision.Common
{
    /// <summary>
    /// The Criteria
    /// </summary>
    public enum Criteria
    {
        /// <summary>
        /// The activity
        /// </summary>
        Activity,

        /// <summary>
        /// The skyline
        /// </summary>
        Skyline,

        /// <summary>
        /// The building height
        /// </summary>
        BuildingHeight,

        /// <summary>
        /// The road signs
        /// </summary>
        RoadSigns,

        /// <summary>
        /// The color variation
        /// </summary>
        ColorVariation
    }
}