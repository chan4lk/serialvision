﻿// ***********************************************************************
// Assembly         : SerialVision.Tests
// Author           : ChandimaRanaweera
// Created          : 10-11-2016
//
// Last Modified By : ChandimaRanaweera
// Last Modified On : 10-11-2016
// ***********************************************************************
// <copyright file="TrainingServiceTests.cs" company="">
//     Copyright ©  2016
// </copyright>
// <summary></summary>
// ***********************************************************************
namespace SerialVision.Tests
{
    using Microsoft.VisualStudio.TestTools.UnitTesting;
    using SerialVision.Services;

    /// <summary>
    /// Class TrainingServiceTests.
    /// </summary>
    [TestClass]
    public class TrainingServiceTests
    {

        /// <summary>
        /// Tests the training.
        /// </summary>
        [TestMethod]
        public void TestTraining()
        {
            ITrainingService service = new TrainingService();
            service.Train();
        }
    }
}
