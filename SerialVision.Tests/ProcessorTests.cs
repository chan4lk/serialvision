﻿// ***********************************************************************
// Assembly         : SerialVision.Tests
// Author           : ChandimaRanaweera
// Created          : 10-09-2016
//
// Last Modified By : ChandimaRanaweera
// Last Modified On : 10-12-2016
// ***********************************************************************
// <copyright file="ProcessorTests.cs" company="SerialVision">
//     Copyright © SerialVision  2016
// </copyright>
// <summary></summary>
// ***********************************************************************
using System;

using SerialVision.MachineLearning;
using SerialVision.Common;
using System.IO;
using CsvHelper;
using System.Collections.Generic;
using System.Linq;
using System.Diagnostics;
using Should;
using System.Drawing;
using SerialVision.ImageProcessing.Helpers;

namespace SerialVision.Tests
{
    using NUnit.Framework;

    using SerialVision.ImageProcessing;

    /// <summary>
    /// Class ProcessorTests.
    /// </summary>
    [TestFixture]
    public class ProcessorTests
    {
        /// <summary>
        /// Tests the method1.
        /// </summary>
        [TestCase]
        public void TestReader()
        {
            using (var sr = new StreamReader(@"..\..\..\Data\Images.csv"))
            {
                var reader = new CsvReader(sr);

                //// CSVReader will now read the whole file into an enumerable
                IEnumerable<ImageInfo> records = reader.GetRecords<ImageInfo>();

                //// First 5 records in CSV file will be printed to the Output Window
                foreach (ImageInfo record in records.ToList())
                {
                    Trace.WriteLine(string.Format("{0} {1}, {2}, {3}", record.URL, record.Skyline, record.Activity,
                        record.BuildingHeight));
                    record.ShouldNotBeNull();
                }

            }

        }

        /// <summary>
        /// Tests the classifier.
        /// </summary>
        [TestCase]
        public void TestClassifier()
        {
            List<Tuple<Bitmap, int>> samples = new List<Tuple<Bitmap, int>>();

            using (var sr = new StreamReader(@"..\..\..\Data\Images.csv"))
            {
                var reader = new CsvReader(sr);

                //// CSVReader will now read the whole file into an enumerable
                IEnumerable<ImageInfo> records = reader.GetRecords<ImageInfo>();
                foreach (ImageInfo record in records.Take(30))
                {
                    using (var image = new Bitmap(record.URL))
                    {
                        samples.Add(new Tuple<Bitmap, int>(image.Resize(100, 100), record.Activity));
                    }
                }

                IClassifier classifier = SvmClassifier.GetInstance();
                classifier.TrainMachine(samples);
                int decision = classifier.Compute(samples[0].Item1);
                decision.ShouldEqual(samples[0].Item2);
            }
        }

        /// <summary>
        /// Tests the classifier with inputs.
        /// </summary>
        [TestCase]
        public void TestClassifierWithInputs()
        {
            double[][] inputs =
            {
                ////               input         output
                new double[] { 0, 1, 1, 0 }, ////  0 
                new double[] { 0, 1, 0, 0 }, ////  0
                new double[] { 0, 0, 1, 0 }, ////  0
                new double[] { 0, 1, 1, 0 }, ////  0
                new double[] { 0, 1, 0, 0 }, ////  0
                new double[] { 1, 0, 0, 0 }, ////  1
                new double[] { 1, 0, 0, 0 }, ////  1
                new double[] { 1, 0, 0, 1 }, ////  1
                new double[] { 0, 0, 0, 1 }, ////  1
                new double[] { 0, 0, 0, 1 }, ////  1
                new double[] { 1, 1, 1, 1 }, ////  2
                new double[] { 1, 0, 1, 1 }, ////  2
                new double[] { 1, 1, 0, 1 }, ////  2
                new double[] { 0, 1, 1, 1 }, ////  2
                new double[] { 1, 1, 1, 1 }, ////  2
            };

            int[] outputs = //// those are the class labels
            {
                0, 0, 0, 0, 0,
                1, 1, 1, 1, 1,
                2, 2, 2, 2, 2,
            };

            IClassifier classifier = SvmClassifier.GetInstance();
            classifier.TrainMachine(inputs, outputs);
            int decision = classifier.Compute(inputs[0]);
            decision.ShouldEqual(outputs[0]);
        }

        /// <summary>
        /// Skylines the test.
        /// </summary>
        /// <param name="path">The path.</param>
        [TestCase(@"D:\Projects\kushan\photo\IMG_8820.jpg")]
        public void SkylineTest(string path)
        {
            IProcessor processor = new ImageProcessor();
            var processed = processor.ProcessSkyLine(new Bitmap(path));
            processed.ShouldNotBeNull();
            processed.Save("D:\\IMG_8820.jpg");

        }
    }
}
