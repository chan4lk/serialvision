﻿namespace SerialVision.Tests
{
    using System.Drawing;

    using NUnit.Framework;

    using SerialVision.Common;
    using SerialVision.Services;

    using Should;

    /// <summary>
    /// Class PredictionServiceTests.
    /// </summary>
    [TestFixture]
    public class PredictionServiceTests
    {
        /// <summary>
        /// Tests the predict.
        /// </summary>
        /// <param name="path">The path.</param>
        /// <param name="criteria">The criteria.</param>
        /// <param name="output">The output.</param>
        [TestCase(@"D:\Projects\kushan\photo\IMG_8820.jpg", Criteria.Activity, 3)]
        public void TestPredict(string path, Criteria criteria, int output)
        {
            IPredictionService service = new PredictionService();
            int computed = service.Predict(new Bitmap(path), criteria);
            computed.ShouldEqual(output);
        }
    }
}