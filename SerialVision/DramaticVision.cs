﻿// ***********************************************************************
// Assembly         : SerialVision
// Author           : ChandimaRanaweera
// Created          : 10-20-2016
//
// Last Modified By : ChandimaRanaweera
// Last Modified On : 10-20-2016
// ***********************************************************************
// <copyright file="DramaticVision.cs" company="">
//     Copyright ©  2016
// </copyright>
// <summary></summary>
// ***********************************************************************

using System;
using System.Drawing;
using System.Windows.Forms;

namespace SerialVision
{
    using System.IO;
    using System.Threading.Tasks;

    using Accord;
    using Accord.Controls;

    using SerialVision.Common;
    using SerialVision.Services;

    using Point = Point;

    /// <summary>
    /// Class DramaticVision.
    /// </summary>
    /// <seealso cref="System.Windows.Forms.Form" />
    public partial class DramaticVision : Form
    {
        /// <summary>
        /// The margin
        /// </summary>
        private const int ChartMargin = 20;

        /// <summary>
        /// The chart.
        /// </summary>
        private Chart chart;

        /// <summary>
        /// The folder name
        /// </summary>
        private string folderName;

        /// <summary>
        /// The test values
        /// </summary>
        private double[,] skylineValues;

        /// <summary>
        /// The building height values.
        /// </summary>
        private double[,] buildingHeightValues;
        
        /// <summary>
        /// The activity values
        /// </summary>
        private double[,] activityValues;

        /// <summary>
        /// The road sign values
        /// </summary>
        private double[,] roadSignValues;

        /// <summary>
        /// The color variation values
        /// </summary>
        private double[,] colorVariationValues;

        /// <summary>
        /// Initializes a new instance of the <see cref="DramaticVision"/> class.
        /// </summary>
        public DramaticVision()
        {
            this.InitializeComponent();
            this.InitializeChart();
        }

        /// <summary>
        /// Initializes the chart.
        /// </summary>
        private void InitializeChart()
        {
            this.chart = new Chart();
            this.chart.Height = this.chartContainer.Height - (ChartMargin * 2);
            this.chart.Width = this.chartContainer.Width - (ChartMargin * 2);
            this.chart.Location = new Point(ChartMargin, ChartMargin);

            this.chartContainer.Controls.Add(this.chart);
        }

        /// <summary>
        /// Handles the Click event of the openFolderToolStripMenuItem1 control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        private void OpenFolderToolStripMenuItemClick(object sender, EventArgs e)
        {
            //// this.folderBrowserDialog.RootFolder = Environment.SpecialFolder.MyComputer;
            this.folderBrowserDialog.ShowNewFolderButton = false;

            // Show the FolderBrowserDialog.
            DialogResult result = this.folderBrowserDialog.ShowDialog();
            if (result == DialogResult.OK)
            {
                this.folderName = this.folderBrowserDialog.SelectedPath;
                this.buildingHeightValues = null;
                this.colorVariationValues = null;
                this.roadSignValues = null;
                this.activityValues = null;
                this.colorVariationValues = null;
            }
        }

        /// <summary>
        /// Processes the folder.
        /// </summary>
        /// <returns>The Task.</returns>
        private async Task ProcessFolder()
        {
            if (string.IsNullOrEmpty(this.folderName))
            {
                MessageBox.Show(this, @"Please Select a folder");
            }
            else
            {
                var files = Directory.GetFiles(this.folderName);
                foreach (var path in files)
                {
                    try
                    {
                        using (var image = new Bitmap(path))
                        {
                            this.processingImage.BackgroundImage = image.Clone() as Bitmap;

                            var response = await this.ProcessImage(image);
                            this.UpdateChart(response);
                        }
                    }
                    catch (Exception exception)
                    {
                        Console.WriteLine("Not an image file {0}", exception.Message);
                    }

                }
            }
        }

        /// <summary>
        /// Processes the image.
        /// </summary>
        /// <param name="image">The image.</param>
        /// <returns>Task Image Process Response </returns>
        private async Task<ImageProcessResponse> ProcessImage(Bitmap image)
        {
            return await Task.Run(
                       () =>
                       {
                           IPredictionService service = new PredictionService();
                           var response = service.Predict(new ImageProcessRequest() { Image = image });
                           return response;
                       });
        }

        /// <summary>
        /// Updates the chart.
        /// </summary>
        /// <param name="data">The data.</param>
        private void UpdateChart(ImageProcessResponse data)
        {
            this.chart.RemoveAllDataSeries();
            
            this.UpdateSingleCurveData(ref this.skylineValues, data.Skyline);
            this.UpdateSingleCurveData(ref this.activityValues, data.ActivityLevel);
            this.UpdateSingleCurveData(ref this.roadSignValues, data.RoadSignsLevel);
            this.UpdateSingleCurveData(ref this.buildingHeightValues, data.BuildingHeight);
            this.UpdateSingleCurveData(ref this.colorVariationValues, data.ColorVariation);

            this.DrawCurve(this.skylineValues, "Sky Line", Color.DeepSkyBlue);
            this.DrawCurve(this.activityValues, "Activity Level", Color.Red);
            this.DrawCurve(this.buildingHeightValues, "Building Height", Color.LimeGreen);
            this.DrawCurve(this.roadSignValues, "Road Signs", Color.Yellow);
            this.DrawCurve(this.colorVariationValues, "Color Variation", Color.Black);
          
        }

        /// <summary>
        /// Draws the curve.
        /// </summary>
        /// <param name="data">The data.</param>
        /// <param name="title">The title.</param>
        /// <param name="color">The color.</param>
        private void DrawCurve(double[,] data, string title, Color color)
        {
            //// add new data series to the chart
            this.chart.AddDataSeries(title, color, Chart.SeriesType.ConnectedDots, 3);

            //// set X range to display
            this.chart.RangeX = new Range(0, data.GetUpperBound(0));

            //// update the chart
            this.chart.UpdateDataSeries(title, data);
        }

        /// <summary>
        /// Updates the single curve data.
        /// </summary>
        /// <param name="curveData">The curve data.</param>
        /// <param name="value">The value.</param>
        private void UpdateSingleCurveData(ref double[,] curveData, double value)
        {
            //// create data series array
            if (curveData == null)
            {
                curveData = new double[2, 2];
            }
            else
            {
                curveData = this.ResizeArray(curveData, curveData.GetUpperBound(0) + 2, 2);
            }

            //// fill data series
            var i = curveData.GetUpperBound(0);
            curveData[i, 0] = i; // X values
            curveData[i, 1] = value; // Y values
        }

        /// <summary>
        /// Resizes the array.
        /// </summary>
        /// <typeparam name="T">The type</typeparam>
        /// <param name="original">The original.</param>
        /// <param name="rows">The rows.</param>
        /// <param name="cols">The cols.</param>
        /// <returns>The new array.</returns>
        private T[,] ResizeArray<T>(T[,] original, int rows, int cols)
        {
            var newArray = new T[rows, cols];
            int minRows = Math.Min(rows, original.GetLength(0));
            int minCols = Math.Min(cols, original.GetLength(1));
            for (int i = 0; i < minRows; i++)
            {
                for (int j = 0; j < minCols; j++)
                {
                    newArray[i, j] = original[i, j];
                }
            }

            return newArray;
        }

        /// <summary>
        /// Handles the Click event of the analyze button control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        private async void AnalyzeBtnClick(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(this.folderName))
            {
                this.OpenFolderToolStripMenuItemClick(this, null);
            }
            else
            {
                this.InitializeProgress();
                await this.ProcessFolder();
                this.StopProgress();
            }
        }

        /// <summary>
        /// Initializes the progress.
        /// </summary>
        private void InitializeProgress()
        {
            this.progressBar.Style = ProgressBarStyle.Marquee;
            this.progressBar.MarqueeAnimationSpeed = 30;
            Application.EnableVisualStyles();
        }

        /// <summary>
        /// Stops the progress.
        /// </summary>
        private void StopProgress()
        {
            this.progressBar.Style = ProgressBarStyle.Continuous;
            this.progressBar.Value = this.progressBar.Maximum;
            this.progressBar.MarqueeAnimationSpeed = 30;
        }
    }
}
