﻿// ***********************************************************************
// Assembly         : SerialVision
// Author           : ChandimaRanaweera
// Created          : 10-09-2016
// Last Modified By : ChandimaRanaweera
// Last Modified On : 10-11-2016
// ***********************************************************************
// <copyright file="Vision.cs" company="SerialVision">
//     Copyright © SerialVision 2016
// </copyright>
// <summary></summary>
// ***********************************************************************
namespace SerialVision
{
    using System;
    using System.Drawing;
    using System.Threading.Tasks;
    using System.Windows.Forms;

    using SerialVision.Common;
    using SerialVision.Services;

    /// <summary>
    ///     Class Vision.
    /// </summary>
    /// <seealso cref="System.Windows.Forms.Form" />
    public partial class Vision : Form
    {
        /// <summary>
        ///     The timer.
        /// </summary>
        private Timer timer;

        /// <summary>
        ///     Initializes a new instance of the <see cref="Vision" /> class.
        /// </summary>
        public Vision()
        {
            this.InitializeComponent();
        }

        /// <summary>
        ///     Analyzes the BTN click.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The <see cref="EventArgs" /> instance containing the event data.</param>
        private async void AnalyzeBtnClick(object sender, EventArgs e)
        {
            if (this.srcImage.BackgroundImage == null)
            {
                this.OpenToolStripMenuItemClick(this, null);
            }

            try
            {
                this.InitializeProgress();
                var response = await this.RequestStatistics();
                this.UpdateStatistics(response);
                this.StopProgress();
            }
            catch (Exception)
            {
                this.StopProgress();
                this.ShowMessage("Prediction failed. Please try again.");
            }
        }

        /// <summary>
        ///     Exit menu item.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs" /> instance containing the event data.</param>
        private void ExitToolStripMenuItemClick(object sender, EventArgs e)
        {
            Application.Exit();
        }

        /// <summary>
        ///     Initializes the progress.
        /// </summary>
        private void InitializeProgress()
        {
            this.ProgressBar.Value = 0;

            this.timer.Tick += (o, args) =>
                {
                    if (this.ProgressBar.Value >= this.ProgressBar.Maximum)
                    {
                        this.ProgressBar.Value = 0;
                    }

                    this.ProgressBar.Value += 20;
                };

            this.timer.Start();
        }

        /// <summary>
        ///     Handles the Click event of the openToolStripMenuItem control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs" /> instance containing the event data.</param>
        private void OpenToolStripMenuItemClick(object sender, EventArgs e)
        {
            this.OpenImage();
        }

        /// <summary>
        /// Opens the image.
        /// </summary>
        private void OpenImage()
        {
            //// Create a new instance of openFileDialog
            var res = new OpenFileDialog();

            //// Filter
            res.Filter = "Image Files|*.jpg;*.jpeg;*.png;*.gif;*.tif;...";

            //// When the user select the file
            if (res.ShowDialog() == DialogResult.OK)
            {
                //// Get the file's path
                var filePath = res.FileName;

                //// Do something
                var image = new Bitmap(filePath);
                this.srcImage.BackgroundImage = image;
            }
        }

        /// <summary>
        ///     Requests the statistics.
        /// </summary>
        /// <returns>The Bitmap.</returns>
        private async Task<ImageProcessResponse> RequestStatistics()
        {
            return await Task.Run(
                       () =>
                           {
                               IPredictionService service = new PredictionService();
                               var image = (Bitmap)this.srcImage.BackgroundImage;
                               var response = service.Predict(new ImageProcessRequest() { Image = image });
                               return response;
                           });
        }

        /// <summary>
        ///     Shows the message.
        /// </summary>
        /// <param name="message">The message.</param>
        private void ShowMessage(string message)
        {
            MessageBox.Show(this, message, "Serial Vision", MessageBoxButtons.OK);
        }

        /// <summary>
        ///     Stops the progress.
        /// </summary>
        private void StopProgress()
        {
            this.ProgressBar.Value = this.ProgressBar.Maximum;
            this.timer.Stop();
        }

        /// <summary>
        ///     Trains the tool strip menu item click.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The <see cref="System.EventArgs" /> instance containing the event data.</param>
        private async void TrainToolStripMenuItemClick(object sender, EventArgs e)
        {

            //// Create a new instance of openFileDialog
            var res = new OpenFileDialog();

            //// Filter
            res.Filter = "CSV Files|*.csv;...";

            //// When the user select the file
            if (res.ShowDialog() == DialogResult.OK)
            {
                //// Get the file's path
                var filePath = res.FileName;
                this.StartTraining();
            }
           
        }

        /// <summary>
        /// Starts the training.
        /// </summary>
        private async void StartTraining()
        {
            try
            {
                this.InitializeProgress();
                await this.Train();
                this.StopProgress();
            }
            catch (Exception)
            {
                this.StopProgress();
                this.ShowMessage("Could not train the data.");
            }
        }

        /// <summary>
        /// Trains this instance.
        /// </summary>
        /// <returns>Task System.Boolean .</returns>
        private async Task<bool> Train()
        {
           return await Task.Run(
                       () =>
                           {
                               ITrainingService service = new TrainingService();
                               service.Train();
                               return true;
                           });
        }

        /// <summary>
        ///     Update the UI with the statistics.
        /// </summary>
        /// <param name="response">The response.</param>
        private void UpdateStatistics(ImageProcessResponse response)
        {
            this.activityLevelTxt.Text = response.ActivityLevel.ToString();
            this.skyLineTxt.Text = response.Skyline.ToString();
            this.buildingHeightTxt.Text = response.BuildingHeight.ToString();
            this.roadSignsTxt.Text = response.RoadSignsLevel.ToString();
            this.colorTxt.Text = response.ColorVariation.ToString();
        }

        /// <summary>
        ///     Handles the Load event of the Vision control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs" /> instance containing the event data.</param>
        private void Vision_Load(object sender, EventArgs e)
        {
            this.timer = new Timer { Interval = 500 };
        }

        /// <summary>
        /// Folders the tool strip menu item click.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        private void FolderToolStripMenuItemClick(object sender, EventArgs e)
        {
            var dramaticForm = new DramaticVision();
            dramaticForm.Show(this);
        }
    }
}