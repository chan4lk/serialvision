﻿// ***********************************************************************
// Assembly         : SerialVision.Services
// Author           : ChandimaRanaweera
// Created          : 10-11-2016
//
// Last Modified By : ChandimaRanaweera
// Last Modified On : 10-11-2016
// ***********************************************************************
// <copyright file="ProcessingService.cs" company="SerialVision">
//     Copyright ©  2016
// </copyright>
// <summary></summary>
// ***********************************************************************

using SerialVision.Common;
using SerialVision.ImageProcessing;

namespace SerialVision.Services
{
    /// <summary>
    /// Class ProcessingService.
    /// </summary>
    /// <seealso cref="SerialVision.Services.IProcessingService" />
    public class ProcessingService : IProcessingService
    {
        /// <summary>
        /// Processes the specified request.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns>Image Process Response.</returns>
        public ImageProcessResponse Process(ImageProcessRequest request)
        {
            IProcessor imageProcessor = new ImageProcessor();
           
            var output = imageProcessor.ProcessActivity(request.Image);

            return new ImageProcessResponse
            {
                Output = output
            };
        }
    }
}
