﻿// ***********************************************************************
// Assembly         : SerialVision.Services
// Author           : ChandimaRanaweera
// Created          : 10-11-2016
//
// Last Modified By : ChandimaRanaweera
// Last Modified On : 10-11-2016
// ***********************************************************************
// <copyright file="TrainingService.cs" company="SerialVision">
//     Copyright © SerialVision 2016
// </copyright>
// <summary></summary>
// ***********************************************************************


namespace SerialVision.Services
{
    using System;
    using System.Collections.Generic;
    using System.Drawing;
    using System.IO;
    using System.Linq;

    using CsvHelper;

    using SerialVision.Common;
    using SerialVision.ImageProcessing;
    using SerialVision.MachineLearning;

    /// <summary>
    /// Class TrainingService.
    /// </summary>
    /// <seealso cref="SerialVision.Services.ITrainingService" />
    public class TrainingService : ITrainingService
    {
        /// <summary>
        /// The image width
        /// </summary>
        private const int ImageWidth = 100;

        /// <summary>
        /// The image height
        /// </summary>
        private const int ImageHeight = 100;

        /// <summary>
        /// Gets or sets the sample data.
        /// </summary>
        /// <value>The sample data.</value>
        public string SampleData { get; set; }

        /// <summary>
        /// Trains this instance.
        /// </summary>
        public void Train()
        {
            var activities = new List<Tuple<Bitmap, int>>();
            var skyline = new List<Tuple<Bitmap, int>>();
            var buildingHeight = new List<Tuple<Bitmap, int>>();
            var roadSigns = new List<Tuple<Bitmap, int>>();
            var colorVariation = new List<Tuple<Bitmap, int>>();

            string dataPath = string.IsNullOrEmpty(this.SampleData) ? ConfigurationData.DataUrl : this.SampleData;

            using (var sr = new StreamReader(dataPath))
            {
                var reader = new CsvReader(sr);

                //// CSVReader will now read the whole file into an enumerable
                IEnumerable<ImageInfo> records = reader.GetRecords<ImageInfo>();
                foreach (var record in records.ToList())
                {
                    using (var image = new Bitmap(record.URL))
                    {
                        IProcessor processor = new ImageProcessor();

                        activities.Add(new Tuple<Bitmap, int>(processor.Process(image, Criteria.Activity), record.Activity));
                        skyline.Add(new Tuple<Bitmap, int>(processor.Process(image, Criteria.Skyline), record.Skyline));
                        buildingHeight.Add(new Tuple<Bitmap, int>(processor.Process(image, Criteria.BuildingHeight), record.BuildingHeight));
                        roadSigns.Add(new Tuple<Bitmap, int>(processor.Process(image, Criteria.RoadSigns), record.RoadSigns));
                        colorVariation.Add(new Tuple<Bitmap, int>(processor.Process(image, Criteria.ColorVariation), record.ColorVariation));
                    }
                }
            }

            this.TrainSingleMachine(activities, Criteria.Activity.ToString());
            this.TrainSingleMachine(skyline, Criteria.Skyline.ToString());
            this.TrainSingleMachine(buildingHeight, Criteria.BuildingHeight.ToString());
            this.TrainSingleMachine(roadSigns, Criteria.RoadSigns.ToString());
            this.TrainSingleMachine(colorVariation, Criteria.ColorVariation.ToString());
        }

        /// <summary>
        /// Trains the single machine.
        /// </summary>
        /// <param name="samples">
        /// The samples.
        /// </param>
        /// <param name="machineName">
        /// Name of the machine.
        /// </param>
        private void TrainSingleMachine(List<Tuple<Bitmap, int>> samples, string machineName)
        {
            IClassifier classifier = SvmClassifier.GetInstance();
            classifier.MachineUrl = string.Format(ConfigurationData.MachineUrlFormat, machineName);
            classifier.TrainMachine(samples);
        }
    }
}