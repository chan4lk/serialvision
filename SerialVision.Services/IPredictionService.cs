﻿// ***********************************************************************
// Assembly         : SerialVision.Services
// Author           : ChandimaRanaweera
// Created          : 10-11-2016
//
// Last Modified By : ChandimaRanaweera
// Last Modified On : 10-11-2016
// ***********************************************************************
// <copyright file="IPredictionService.cs" company="">
//     Copyright ©  2016
// </copyright>
// <summary></summary>
// ***********************************************************************
namespace SerialVision.Services
{
    using System.Drawing;

    using SerialVision.Common;

    /// <summary>
    /// Interface IPredictionService
    /// </summary>
    public interface IPredictionService
    {
        /// <summary>
        /// Predicts the specified image.
        /// </summary>
        /// <param name="image">The image.</param>
        /// <param name="criteria">The criteria.</param>
        /// <returns>The predicted output.</returns>
        int Predict(Bitmap image, Criteria criteria);

        /// <summary>
        /// Predicts the specified request.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns>Image Process Response.</returns>
        ImageProcessResponse Predict(ImageProcessRequest request);
    }
}