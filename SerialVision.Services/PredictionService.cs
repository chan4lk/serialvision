﻿namespace SerialVision.Services
{
    using System.Drawing;

    using SerialVision.Common;
    using SerialVision.ImageProcessing;
    using SerialVision.MachineLearning;

    /// <summary>
    /// Class PredictionService.
    /// </summary>
    /// <seealso cref="SerialVision.Services.IPredictionService" />
    public class PredictionService : IPredictionService
    {
        /// <summary>
        /// Predicts the specified image.
        /// </summary>
        /// <param name="image">The image.</param>
        /// <param name="criteria">The criteria.</param>
        /// <returns>The predicted output.</returns>
        public int Predict(Bitmap image, Criteria criteria)
        {
            IClassifier machine = SvmClassifier.GetInstance();
            machine.MachineUrl = string.Format(ConfigurationData.MachineUrlFormat, criteria.ToString());
            machine.TrainMachine(machine.MachineUrl);

            IProcessor processor = new ImageProcessor();
            Bitmap computed = processor.Process(image, criteria);

            int prediction = machine.Compute(computed);
            return prediction;
        }

        /// <summary>
        /// Predicts the specified request.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns>Image Process Response.</returns>
        public ImageProcessResponse Predict(ImageProcessRequest request)
        {
            var image = request.Image;
            var activity = this.Predict(image, Criteria.Activity);
            var buildingHeight = this.Predict(image, Criteria.BuildingHeight);
            var roadSigns = this.Predict(image, Criteria.RoadSigns);
            var skyline = this.Predict(image, Criteria.Skyline);
            var colorVariation = this.Predict(image, Criteria.ColorVariation);
            var response = new ImageProcessResponse
            {
                Output = image,
                ActivityLevel = activity,
                ColorVariation = colorVariation,
                RoadSignsLevel = roadSigns,
                Skyline = skyline,
                BuildingHeight = buildingHeight
            };

            return response;
        }
    }
}