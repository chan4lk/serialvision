﻿// ***********************************************************************
// Assembly         : SerialVision.Services
// Author           : ChandimaRanaweera
// Created          : 10-11-2016
//
// Last Modified By : ChandimaRanaweera
// Last Modified On : 10-11-2016
// ***********************************************************************
// <copyright file="ITrainingService.cs" company="SerialVision">
//     Copyright © SerialVision 2016
// </copyright>
// <summary></summary>
// ***********************************************************************

namespace SerialVision.Services
{
    /// <summary>
    /// Interface ITrainingService
    /// </summary>
    public interface ITrainingService
    {
        /// <summary>
        /// Gets or sets the sample data.
        /// </summary>
        /// <value>The sample data.</value>
        string SampleData { get; set; }

        /// <summary>
        /// Trains this instance.
        /// </summary>
        void Train();
    }
}
