﻿// ***********************************************************************
// Assembly         : SerialVision.Services
// Author           : ChandimaRanaweera
// Created          : 10-11-2016
//
// Last Modified By : ChandimaRanaweera
// Last Modified On : 10-11-2016
// ***********************************************************************
// <copyright file="IProcesingService.cs" company="SerialVision">
//     Copyright ©  2016
// </copyright>
// <summary></summary>
// ***********************************************************************
using SerialVision.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SerialVision.Services
{
    /// <summary>
    /// Interface IProcesingService
    /// </summary>
    public interface IProcessingService
    {
        /// <summary>
        /// Processes the specified request.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns>ImageProcessResponse.</returns>
        ImageProcessResponse Process(ImageProcessRequest request);
    }
}
