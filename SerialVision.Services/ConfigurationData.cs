﻿namespace SerialVision.Services
{
    using System.Configuration;

    /// <summary>
    /// The configuration data.
    /// </summary>
    internal static class ConfigurationData
    {

        /// <summary>
        /// Gets the data URL.
        /// </summary>
        /// <value>The data URL.</value>
        public static string DataUrl
        {
            get
            {
                string load = ConfigurationManager.AppSettings["dataUrl"];
                return load;
            }
        }

        /// <summary>
        /// Gets the machine URL format.
        /// </summary>
        /// <value>The machine URL format.</value>
        public static string MachineUrlFormat
        {
            get
            {
                string load = ConfigurationManager.AppSettings["machineUrlFormat"];
                return load;
            }
        }
    }
}
