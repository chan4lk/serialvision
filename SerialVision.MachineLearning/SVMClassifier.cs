﻿namespace SerialVision.MachineLearning
{
    using System;
    using System.Collections.Generic;
    using System.Drawing;

    using Accord.Imaging.Converters;
    using Accord.IO;
    using Accord.MachineLearning.VectorMachines;
    using Accord.MachineLearning.VectorMachines.Learning;
    using Accord.Statistics.Kernels;

    /// <summary>
    /// The SVM classifier.
    /// </summary>
    public class SvmClassifier : IClassifier
    {
        /// <summary>
        /// The inputs count.
        /// </summary>
        private int inputsCount;

        /// <summary>
        /// Prevents a default instance of the <see cref="SvmClassifier"/> class from being created.
        /// </summary>
        private SvmClassifier()
        {
        }

        /// <summary>
        /// Gets or sets the inputs.
        /// </summary>
        /// <value>
        /// The inputs.
        /// </value>
        public List<double[]> Inputs { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether this instance is trained.
        /// </summary>
        /// <value>
        /// <c>true</c> if this instance is trained; otherwise, <c>false</c>.
        /// </value>
        public bool IsTrained { get; set; }

        /// <summary>
        /// Gets or sets the outputs.
        /// </summary>
        /// <value>
        /// The outputs.
        /// </value>
        public List<int> Outputs { get; set; }

        /// <summary>
        /// Gets or sets the samples.
        /// </summary>
        /// <value>
        /// The samples.
        /// </value>
        public List<Tuple<Bitmap, int>> Samples { get; set; }

        /// <summary>
        /// Gets or sets the machine.
        /// </summary>
        private MulticlassSupportVectorMachine<IKernel> Machine { get; set; }

        /// <summary>
        /// Gets or sets the machine URL.
        /// </summary>
        /// <value>The machine URL.</value>
        public string MachineUrl { get; set; }

        /// <summary>
        /// Gets the instance.
        /// </summary>
        /// <returns>
        /// The instance.
        /// </returns>
        public static SvmClassifier GetInstance()
        {
            return new SvmClassifier();
        }

        /// <summary>
        /// Computes the specified image.
        /// </summary>
        /// <param name="image">
        /// The image.
        /// </param>
        /// <returns>
        /// The decision.
        /// </returns>
        public int Compute(Bitmap image)
        {
            //// Compute the decision output for one of the input vectors
            double[] input = this.Extract(image);
            int decision = this.Machine.Decide(input);
            return decision;
        }

        /// <summary>
        /// Computes the specified input.
        /// </summary>
        /// <param name="input">
        /// The input.
        /// </param>
        /// <returns>
        /// The computed value.
        /// </returns>
        public int Compute(double[] input)
        {
            // Compute the decision output for one of the input vectors
            int decision = this.Machine.Decide(input);
            return decision;
        }

        /// <summary>
        /// Trains the machine.
        /// </summary>
        /// <param name="url">
        /// The URL.
        /// </param>
        public void TrainMachine(string url)
        {
            if (ConfigurationData.LoadMachineFromFile)
            {
                this.Machine = Serializer.Load<MulticlassSupportVectorMachine<IKernel>>(url);
                this.IsTrained = true;
            }
            else
            {
                this.TrainMachine(this.Samples);
            }
        }

        /// <summary>
        /// Trains the machine.
        /// </summary>
        public void TrainMachine()
        {
            this.TrainMachine(this.Samples);
        }

        /// <summary>
        /// Trains the machine.
        /// </summary>
        /// <param name="data">The data.</param>
        public void TrainMachine(List<Tuple<Bitmap, int>> data)
        {
            this.Inputs = new List<double[]>();
            this.Outputs = new List<int>();

            foreach (Tuple<Bitmap, int> tuple in data)
            {
                double[] features = this.Extract(tuple.Item1);
                if (features != null && features.Length != 0)
                {
                    this.Inputs.Add(features);
                    this.Outputs.Add(tuple.Item2);
                }
            }

            this.TrainMachine(this.Inputs.ToArray(), this.Outputs.ToArray());
        }

        /// <summary>
        /// Trains the Machine.
        /// </summary>
        /// <param name="inputs">The inputs.</param>
        /// <param name="outputs">The outputs.</param>
        /// <exception cref="System.ArgumentException">
        /// Inputs cannot be null
        /// or
        /// outputs cannot be null
        /// </exception>
        public void TrainMachine(double[][] inputs, int[] outputs)
        {
            if (inputs == null)
            {
                throw new ArgumentException("Inputs cannot be null");
            }

            if (outputs == null)
            {
                throw new ArgumentException("outputs cannot be null");
            }

            // Create a new polynomial kernel
            IKernel kernel = new Polynomial(2);

            // Create the Multi-class learning algorithm for the Machine
            var teacher = new MulticlassSupportVectorLearning<IKernel>
            {
                // Configure the learning algorithm to use SMO to train the
                // underlying SVMs in each of the binary class subproblems.
                Learner =
                                      param =>
                                          new SequentialMinimalOptimization<IKernel>
                                          {
                                              // Estimate a suitable guess for the Gaussian kernel's parameters.
                                              // This estimate can serve as a starting point for a grid search.
                                              UseKernelEstimation = true,
                                              Kernel = kernel
                                          },
                ParallelOptions = { MaxDegreeOfParallelism = 1 }
            };

            //// teacher.Kernel = kernel;
            this.Machine = teacher.Learn(inputs, outputs);

            this.Machine.Save(this.MachineUrl ?? ConfigurationData.MachineUrl);

            this.IsTrained = true;
        }

        /// <summary>
        /// Extracts the specified BMP.
        /// </summary>
        /// <param name="image">The image.</param>
        /// <returns>
        /// Extracted features.
        /// </returns>
        private double[] Extract(Bitmap image)
        {
            ImageToArray converter = new ImageToArray(min: -1, max: +1);

            double[] input;

            converter.Convert(image, out input);

            this.inputsCount = input.Length;

            return input;
        }
    }
}