﻿// ***********************************************************************
// Assembly         : SerialVision.MachineLearning
// Author           : ChandimaRanaweera
// Created          : 10-09-2016
//
// Last Modified By : ChandimaRanaweera
// Last Modified On : 10-09-2016
// ***********************************************************************
// <copyright file="KSVM.cs" company="">
//     Copyright ©  2016
// </copyright>
// <summary></summary>
// ***********************************************************************
using Accord.Controls;
using Accord.MachineLearning.VectorMachines;
using Accord.MachineLearning.VectorMachines.Learning;

namespace SerialVision.MachineLearning
{
    /// <summary>
    /// Class KSVM.
    /// </summary>
    public class KSVM
    {
        /// <summary>
        /// Processes this instance.
        /// </summary>
        public void Process()
        {

            // As an example, we will try to learn a linear machine  that can 
            // replicate the "exclusive-or" logical function. However, since we
            // will be using a linear SVM, we will not be able to solve this 
            // problem perfectly as the XOR is a non-linear classification problem:
            double[][] inputs =
            {
                new double[] { 0, 0 }, // the XOR function takes two booleans
                new double[] { 0, 1 }, // and computes their exclusive or: the
                new double[] { 1, 0 }, // output is true only if the two booleans
                new double[] { 1, 1 }  // are different
            };

            int[] xor = // this is the output of the xor function
            {
                0, // 0 xor 0 = 0 (inputs are equal)
                1, // 0 xor 1 = 1 (inputs are different)
                1, // 1 xor 0 = 1 (inputs are different)
                0, // 1 xor 1 = 0 (inputs are equal)
            };

            // Now, we can create the sequential minimal optimization teacher
            var learn = new SequentialMinimalOptimization()
            {
                UseComplexityHeuristic = true,
                UseKernelEstimation = false
            };

            // And then we can obtain a trained SVM by calling its Learn method
            SupportVectorMachine svm = learn.Learn(inputs, xor);

            // Finally, we can obtain the decisions predicted by the machine:
            bool[] prediction = svm.Decide(inputs);

            //// Console.WriteLine("error: " + error);

            //// Show results on screen
            ScatterplotBox.Show("Training data", inputs, xor);

            //// ScatterplotBox.Show("SVM results", inputs.GetColumn(0),
            ////    inputs.Apply(p => ksvm.Compute(p)));

            //// Console.ReadKey();
        }
    }

}
